use strict;
use warnings;

hello();
my $avg = average(10,20,30);
print "Average of 10, 20, 30 is equal to $avg.\n";

my $DNA = <>;
chomp ($DNA);
$DNA = myfunc($DNA);
print $DNA,"\n";


sub hello {
	print "hello\n"
}

sub average {
	my $size = scalar @_;
	my $total = 0;

	for (my $i=0; $i<$size; $i++){
		$total += $_[$i];}
	my $avg = $total/$size;
	return $avg;
 
}

sub myfunc {
	my ($DNA = @_;)
	if ($DNA =~ /atg/){
		$DNA =~ s/atg/ATG/g;}
	return $DNA;
}

sub myfunc2 {
	my $DNAref = shift;
	if ($DNAref =~ /atg/){
		$DNAref =~ s/atg/ATG/g;}
}